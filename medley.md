Install VirtualBox (eg on Mac: `brew cask install virtualbox`)

Download Debian 3.1 from here:
https://virtualboxes.org/images/debian/

Create a new VM in VirtualBox and select the vdi file from the above archive as a disk.
Check the VM settings:
- Make sure the disk is mounted as an IDE device, SATA errors out on boot (in the Storage tab)
- Also, make sure the pointing device is set to PS/2 Mouse (in the General tab)

Start the VM. Log in as `root`/`toor`.

X11 needs to be installed.
Edit the sources-list (`/etc/apt/sources.list`) with eg `nano` by commenting out everything and adding:
`deb http://archive.debian.org/debian/ sarge contrib main non-free`

To install X11, execute:

`apt-get update`

`apt-get install x-window-system`

Configure X11:

`dpkg-reconfigure xserver-xfree86`

Most important step here is to set the mouse as a PS/2 device, else X won't boot.
I also chose a relatively low video resolution, eg 800x600, 60 Hz/16 bit, don't know if it's necessary.

To download the LFG software:

`wget ftp://ftp.parc.xerox.com/pub/lfg/lfg.sysout`

`wget ftp://ftp.parc.xerox.com/pub/lfg/linux/intel/lde` (not sure if necessary)

`wget ftp://ftp.parc.xerox.com/pub/lfg/linux/intel/ldex`

`chmod +x lde`

`chmod +x ldex`

Run X:

`startx`

Now click anywhere and select Debian -> XShells -> XTerm

Now run `./ldex lfg.sysout`. 

This should start up Medley. After a while, a kind of REPL will show up.


